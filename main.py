import src.viewers as viewers
import src.models as models


class WordleGameRunner:

    viewer = viewers.CLI
    game_model = models.WordleGame

    @classmethod
    def run(cls):
        wordle_game = cls.game_model()
        cls.viewer.print_rules()
        cls.viewer.print_game_state(wordle_game)
        while not wordle_game.is_finished:
            guess = cls.viewer.get_guess()
            wordle_game.apply_guess(guess)
            cls.viewer.print_game_state(wordle_game)

        cls.viewer.print_result(wordle_game)


def main():
    game_controller = WordleGameRunner()
    game_controller.run()


if __name__ == "__main__":
    main()