import pytest

from context import models


@pytest.fixture
def wordle_game():
    game = models.WordleGame()
    return game


def test_wordle_game_with_win(wordle_game):
    wordle_game.answer = "STEAM"
    assert wordle_game.word_length == 5
    assert wordle_game.is_finished is False
    assert wordle_game.is_guessed is False
    assert wordle_game.num_guesses_made == 0
    assert wordle_game.letters_guessed == set()
    assert wordle_game.letters == [
        models.Letter('S', ' ', models.LetterStates.INCORRECT),
        models.Letter('T', ' ', models.LetterStates.INCORRECT),
        models.Letter('E', ' ', models.LetterStates.INCORRECT),
        models.Letter('A', ' ', models.LetterStates.INCORRECT),
        models.Letter('M', ' ', models.LetterStates.INCORRECT)
    ]

    wordle_game.apply_guess("STOKE")
    assert wordle_game.word_length == 5
    assert wordle_game.is_finished is False
    assert wordle_game.is_guessed is False
    assert wordle_game.num_guesses_made == 1
    assert wordle_game.letters_guessed == {"S", "T", "O", "K", "E"}
    assert wordle_game.letters == [
        models.Letter('S', 'S', models.LetterStates.CORRECT),
        models.Letter('T', 'T', models.LetterStates.CORRECT),
        models.Letter('E', 'O', models.LetterStates.INCORRECT),
        models.Letter('A', 'K', models.LetterStates.INCORRECT),
        models.Letter('M', 'E', models.LetterStates.MISPLACED)
    ]

    wordle_game.apply_guess("STEAM")
    assert wordle_game.word_length == 5
    assert wordle_game.is_finished is True
    assert wordle_game.is_guessed is True
    assert wordle_game.num_guesses_made == 2
    assert wordle_game.letters_guessed == {"S", "T", "O", "K", "E", "A", "M"}
    assert wordle_game.letters == [
        models.Letter('S', 'S', models.LetterStates.CORRECT),
        models.Letter('T', 'T', models.LetterStates.CORRECT),
        models.Letter('E', 'E', models.LetterStates.CORRECT),
        models.Letter('A', 'A', models.LetterStates.CORRECT),
        models.Letter('M', 'M', models.LetterStates.CORRECT)
    ]
    

def test_wordle_game_run_out_of_guesses(wordle_game):
    wordle_game.answer = "PROXY"
    assert wordle_game.word_length == 5
    assert wordle_game.is_finished is False
    assert wordle_game.is_guessed is False
    assert wordle_game.num_guesses_made == 0
    assert wordle_game.num_guesses_limit == 6
    assert wordle_game.letters_guessed == set()
    assert wordle_game.letters == [
        models.Letter('P', ' ', models.LetterStates.INCORRECT),
        models.Letter('R', ' ', models.LetterStates.INCORRECT),
        models.Letter('O', ' ', models.LetterStates.INCORRECT),
        models.Letter('X', ' ', models.LetterStates.INCORRECT),
        models.Letter('Y', ' ', models.LetterStates.INCORRECT)
    ]

    for guess_number in range(1, 6):
        wordle_game.apply_guess("AAAAA")
        assert wordle_game.is_finished is False
        assert wordle_game.is_guessed is False
        assert wordle_game.num_guesses_made == guess_number
        assert wordle_game.letters_guessed == {"A"}
        assert wordle_game.letters == [
            models.Letter('P', 'A', models.LetterStates.INCORRECT),
            models.Letter('R', 'A', models.LetterStates.INCORRECT),
            models.Letter('O', 'A', models.LetterStates.INCORRECT),
            models.Letter('X', 'A', models.LetterStates.INCORRECT),
            models.Letter('Y', 'A', models.LetterStates.INCORRECT)
        ]

    wordle_game.apply_guess("AAAAA")
    assert wordle_game.is_finished is True
    assert wordle_game.is_guessed is False
    assert wordle_game.num_guesses_made == 6
    assert wordle_game.letters_guessed == {"A"}
    assert wordle_game.letters == [
        models.Letter('P', 'A', models.LetterStates.INCORRECT),
        models.Letter('R', 'A', models.LetterStates.INCORRECT),
        models.Letter('O', 'A', models.LetterStates.INCORRECT),
        models.Letter('X', 'A', models.LetterStates.INCORRECT),
        models.Letter('Y', 'A', models.LetterStates.INCORRECT)
    ]
