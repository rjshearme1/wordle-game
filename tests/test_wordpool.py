from context import models


def test_word_pool():
    word = models.WordPool.get_random_word()
    assert models.WordPool._words != set()
    assert len(word) == 5
    assert isinstance(word, str)
    assert len(models.WordPool.get_words()) == 2312