# Wordle Game
This is a python implementation of a CLI version of the popular game Wordle.
The online version can be found [here](https://www.nytimes.com/games/wordle/index.html).

## Scope
The implementation does the following:
1. Explains the rules to the player
2. Displays the game state each turn including state of letters guessed and letters remaining to guess
3. Evaluates if the game is won or lost
4. Interacts with the player via a CLI

## Quickstart
To run the game install the `requirements.txt` to a Python3.6+ environment and then run `python main.py`.
The game will then launch and instructions presented on screen.


## Running tests:
Tests can be run from the top directory with "pytest".

## Extension questions
1. How would you implement a CLI interface so that you could define what type of game you want to play?
2. How would you implement hard mode in the game? (Hard mode is where any correct letters must be used in subsequent guesses in the correct place and any misplaced letters must be used in any position) 