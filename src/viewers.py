import string

from termcolor import colored

from src import models

WORDLE_RULES_TEXT =  """Your aim is to guess a 5 letter word within 6 attempts. Once you guess a word, if a letter is 
correct and in the correct position the letter will show as green on the board. If a letter is correct but in the 
wrong position it will show in yellow. Once you have all 5 letters guessed correctly you win. If you
take more than 6 attempts you will lose.

To guess a word enter a 5-letter word at the prompt '>'

"""


class CLI:

    prompt = "Guess a word: "

    @classmethod
    def clear_line(cls):
        print("\033[A\r" + " " * 76 + "\r", end="")

    @classmethod
    def get_guess(cls):
        guess = cls.read_input()
        while guess not in models.WordPool.get_words():
            guess = cls.read_input()
        return guess

    @classmethod
    def read_input(cls):
        raw_guess = input("> ")
        cls.clear_line()
        return raw_guess.strip().upper()

    @classmethod
    def print_rules(cls):
        print("--- Wordle ---")
        print(WORDLE_RULES_TEXT)

    @classmethod
    def print_game_state(cls, wordle_game):
        cls.print_word_state(wordle_game)
        print(" " * 10, end="")
        cls.print_letters_guessed(wordle_game)
        print("\n", end="")

    @classmethod
    def print_word_state(cls, word):
        for letter in word.letters:
            if letter.state == models.LetterStates.CORRECT:
                on_color = "on_green"
            elif letter.state == models.LetterStates.MISPLACED:
                on_color = "on_yellow"
            else:
                on_color = None
            print(colored(f"[{letter.guess}]", on_color=on_color), end=" ")

    @classmethod
    def print_letters_guessed(cls, word):
        for letter in string.ascii_uppercase:
            color = None
            if letter in word.letters_guessed:
                color = "grey"
            print(colored(letter, color=color), end=" ")

    @classmethod
    def print_result(cls, wordle_game):
        if wordle_game.is_guessed:
            print(f"Congratulations, you won in {wordle_game.num_guesses_made} guesses!")
        else:
            print(f"Commiserations, you lost. The answer was: {wordle_game.answer}")
