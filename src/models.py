import enum
import collections
import random


class LetterStates(enum.Enum):
    INCORRECT = 0
    MISPLACED = 1
    CORRECT = 2


Letter = collections.namedtuple("Letter", "answer guess state")


class WordPool:

    _words = set()

    @classmethod
    def get_words(cls):
        if cls._words:
            return cls._words
        with open("resources/words.txt") as fh:
            cls._words = set(fh.read().splitlines())
        return cls._words

    @classmethod
    def get_random_word(cls):
        chosen_word = random.choice(list(cls.get_words()))
        return chosen_word


class WordleGame:

    def __init__(self):
        self.answer = WordPool.get_random_word()
        self.last_guess_made = " " * 5
        self.letters_guessed = set()
        self.num_guesses_made = 0
        self.num_guesses_limit = 6

    @property
    def is_finished(self):
        return self.num_guesses_made >= self.num_guesses_limit or self.is_guessed

    @property
    def word_length(self):
        return len(self.answer)

    @property
    def is_guessed(self):
        return self.answer == self.last_guess_made

    @property
    def letters(self):
        letters = [None] * self.word_length
        answer_letter_counts = collections.Counter(self.answer)

        for index, (answer_letter, guess_letter) in enumerate(zip(self.answer, self.last_guess_made)):
            if guess_letter == answer_letter:
                letter_state = LetterStates.CORRECT
                answer_letter_counts[guess_letter] -= 1
            else:
                letter_state = LetterStates.INCORRECT
            letters[index] = Letter(answer_letter, guess_letter, letter_state)

        for index, (answer_letter, guess_letter) in enumerate(zip(self.answer, self.last_guess_made)):
            if answer_letter_counts[guess_letter] > 0:
                letter_state = LetterStates.MISPLACED
                answer_letter_counts[guess_letter] -= 1
                letters[index] = Letter(answer_letter, guess_letter, letter_state)

        return letters

    def apply_guess(self, guess_value):
        self.last_guess_made = guess_value
        self.letters_guessed.update(set(guess_value))
        self.num_guesses_made += 1
